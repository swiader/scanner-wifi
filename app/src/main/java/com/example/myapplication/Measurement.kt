package com.example.myapplication

import java.util.*

data class Measurement(
    val id: Int,
    val value: Double,
    val type: String,
    val date: String,
    val indoor: Boolean
) {
    override fun toString(): String {
        return "$id,$value,$type,$date,$indoor"
    }
}