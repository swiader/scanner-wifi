package com.example.myapplication

import java.io.FileWriter
import java.io.IOException

class CsvService(measurements: List<Measurement>) {
    private val measurements = measurements

    //todo: import external library
    //https://github.com/doyaaaaaken/kotlin-csv
    //lateinit var csvWriter: CSVWriter()
    private val CSV_HEADER = "id,value,type,date,indoor"

    fun writeCsv(): Boolean {
        var fileWriter: FileWriter? = null

        try {
            fileWriter = FileWriter("dupa.csv")
            fileWriter.append(CSV_HEADER)
            fileWriter.append('\n')
            for (measure in measurements) {
                fileWriter.append(measure.toString())
                fileWriter.append('\n')
            }
            println("Csv wrote successfully")
        } catch (e: Exception) {
            println("Writing csv error")
            e.printStackTrace()
        } finally {
            try {
                fileWriter!!.flush()
                fileWriter.close()
                return true;
            } catch (e: IOException) {
                println("Flushing error")
                e.printStackTrace()
                return false;
            }
        }
    }
}