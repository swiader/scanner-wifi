package com.example.myapplication

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.net.wifi.WifiManager
import android.os.Build
import android.telephony.*
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import java.lang.Exception
import java.text.DateFormat
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timer
import kotlin.time.measureTime

class MainActivity : AppCompatActivity() {

    //Android Dependencies
    private var mainWifi: WifiManager? = null
    private var telephonyManager: TelephonyManager? = null

    private var textLog: TextView? = null //trash

    //Others
    private var timerCapture: Timer? = null

    //UI Elements
    lateinit var startButton: Button
    lateinit var stopButton: Button

    //Measurements Elements
    lateinit var csvFileName: String
    lateinit var measuresData: ArrayList<String>
    var numDataPoints: Int = 0

    //Measured Values
    var lteStr: String = ""
    lateinit var parts: List<String>
    var cellPci: Int = 0
    var cellGsm: Int = 0
    private var indoor: Boolean = true
    private var isRecording = false;

    //measures
    var measures: ArrayList<Measurement> = arrayListOf<Measurement>()
    var wifis: ArrayList<Measurement> = arrayListOf<Measurement>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainWifi = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

        registerReceiver(wifiScanReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
        mainWifi!!.startScan()
        telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

        telephonyManager!!.listen(signalStrenghListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS)
        telephonyManager!!.listen(signalStrenghListener, PhoneStateListener.LISTEN_CELL_INFO)

        assignButtons()
        assignListeners()
    }

    private fun assignButtons() {
        startButton = findViewById(R.id.btn_start)
        stopButton = findViewById(R.id.btn_stop)
    }

    fun onStartRecording() {
        isRecording = true;
        startButton.isEnabled = false;
        stopButton.isEnabled = true;

        csvFileName = DateFormat
            .getDateInstance()
            .format(Calendar.getInstance().time)
            .replace(' ', '_')
            .replace(",", "")

        measuresData = ArrayList<String>()
        createDataCaptureTimer()
    }

    private fun createDataCaptureTimer() {

        numDataPoints = 0
        timerCapture = Timer()
        timerCapture!!.scheduleAtFixedRate(
            object : TimerTask() {
                var rsrp: Int = 0
                var rsrq: Int = 0
                var cqi: Int = 0
                var id = 1

                override fun run() {
                    if (::parts.isInitialized) {
                        return
                    } else {
                        rsrp = Integer.parseInt(parts[9])
                        rsrq = Integer.parseInt(parts[10])
                        cqi = Integer.parseInt(parts[12])

                        if (rsrp == 2147483647) {
                            rsrp = -141
                        }
                        if (rsrq == 2147483647) {
                            rsrq = -30;
                        }
                        if (cqi == 2147483647) {
                            cqi = -20
                        }

                        val mt = DateFormat.getDateTimeInstance().format(
                            Calendar.getInstance().time
                        )

                        measures.add(
                            Measurement(
                                id,
                                rsrp.toDouble(),
                                "RSRP",
                                mt.toString(),
                                indoor
                            )
                        )
                        measures.add(
                            Measurement(
                                id,
                                rsrq.toDouble(),
                                "RSRQ",
                                mt.toString(),
                                indoor
                            )
                        )
                        measures.add(Measurement(id, cqi.toDouble(), "CQI", mt.toString(), indoor))

                        if (wifis.size > 0) {
                            wifis.forEach {
                                measures.add((it))
                            }
                        }
                    }
                }
            },
            0,
            1000
        )
    }

    fun onPauseResumeRecording() {

    }

    fun onStopRecording() {
        if (timerCapture == null) {
            return;
        }

        timerCapture!!.cancel()
        timerCapture!!.purge()
        timerCapture = null;

        isRecording = false;
        startButton.isEnabled = true;
        stopButton.isEnabled = false;
    }

    fun switchOutdoorIndoor() {
        indoor = !indoor
        //change text
    }

    fun getConnectedWifiData(): Int {
        var rrsi: Int = 0

        try {
            rrsi = mainWifi!!.connectionInfo.rssi
        } catch (e: Exception) {
            rrsi = -1
            e.printStackTrace()
        }

        return rrsi
    }

    override fun onPause() {
        try {
            telephonyManager!!.listen(signalStrenghListener, PhoneStateListener.LISTEN_NONE)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.onPause()
    }

    override fun onDestroy() {
        try {
            telephonyManager!!.listen(signalStrenghListener, PhoneStateListener.LISTEN_NONE)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        unregisterReceiver(wifiScanReceiver)
        super.onDestroy()
    }

    private fun assignListeners() {
        findViewById<Button>(R.id.btn_start).setOnClickListener {
            mainWifi = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            println(mainWifi!!.scanResults.size)
        }
    }

    //pobiera dane z wifi skanera
    //dane moga byc lekko zaburzone https://stackoverflow.com/questions/49178307/startscan-in-wifimanager-deprecated-in-android-p https://stackoverflow.com/questions/56401057/wifimanager-startscan-deprecated-alternative
    val wifiScanReceiver = object : BroadcastReceiver() {

        override fun onReceive(p0: Context?, p1: Intent?) {

            if (wifis.size > 0) {
                wifis.clear()
                wifis.removeAt(0)
            }

            val getLevels = mainWifi!!.scanResults
            when (p1?.action) {
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION -> getLevels.forEach {
                    textLog?.text = "" + it.level.toString() + "\n" + textLog?.text
                    println(it.level.toString() + it.SSID)
                    wifis.add(
                        Measurement(
                            0,
                            it.level.toDouble(),
                            "WIFI: " + it.SSID,
                            DateFormat.getDateTimeInstance().format(
                                Calendar.getInstance().time
                            ).toString(),
                            indoor
                        )
                    )
                }
            }

        }
    }

    //pobiera sygnały
    private val signalStrenghListener = object : PhoneStateListener() {
        @SuppressLint("MissingPermission")
        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
        override fun onSignalStrengthsChanged(signalStrength: SignalStrength?) {
            lteStr = signalStrength.toString()
            parts = lteStr.split(" ")
            try {
                telephonyManager!!.allCellInfo.forEach {

                    if (it is CellInfoLte) {
                        cellPci = it.cellIdentity.pci
                    }

                    if (it is CellInfoGsm) {
                        cellGsm = it.cellSignalStrength.dbm
                    }

                }
            } catch (e: Exception) {
                Log.d("SignalStrengh", e.stackTrace.toString())
            }
            super.onSignalStrengthsChanged(signalStrength)
        }
    }

}
